{% args status %}
<html>
  <head>
    <meta charset="UTF-8">
    <title>Domótica v4</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="main.css" rel="stylesheet" type="text/css" media="all">
  </head>
  
  <body>
    <div class="navbar">
      <h2>Domótica</h2>
    </div>
    
    
    <div class="main">
    
      {% for grupo in status %}
      <div class="group-entry">
        <div class="group-title">
          {{ grupo[3] }}
        </div>
        
        <div class="group-buttons">
          <div class="group-button group-button-on {% if grupo[1] == 1 %} disabled {% endif %}">
            <form action="/api/{{ grupo[0] }}/on" method="post">
                <input type="submit" value="Encender" ></input>
            </form>
          </div>
          
          <div class="group-button group-button-off {% if grupo[1] == 0 %} disabled {% endif %}">
            <form action="/api/{{ grupo[0] }}/off" method="post">
                <input type="submit" value="Apagar" ></input>
            </form>
          </div>
        </div>
      </div>
      {% endfor %}
    
      <div class="links">
        <a href="/ajustes"><p>Ajustes</p></a>
        <a href="/about"><p>Acerca de</p></a>
      </div>
    
    </div>
  </body>
</html>
