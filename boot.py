# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)

import picoweb
import random
import ure as re
import ujson as json
from machine import Pin
from neopixel import NeoPixel

def do_connect():
    import network
    wlan = network.WLAN(network.STA_IF)
    network.WLAN(network.AP_IF).active(False)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect('VirusMaligno-R', 'criamacho')
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())

r1 = Pin(12,Pin.OUT,value=0)
r2 = Pin(13,Pin.OUT,value=0)

do_connect()

def update_output():
    for _,value,_,_,func,args in status:
        func(value,args)
        
def update_gpio(value,args):
    if value == 0:
        args[0].off()
    else:
        args[0].on()

#status = [id,initial_value,actions,label,update_func,args_update_func]
status = [
    ["jardin",0,["on","off"],"Jardín.",update_gpio,(r1,)],
    ["sotano",0,["on","off"],"Sótano.",update_gpio,(r2,)],
]

update_output()

def index(req, resp):
    yield from picoweb.start_response(resp)
    yield from app.render_template(resp, "index.tpl", (status,))

def api(req,resp):
    yield from picoweb.start_response(resp,"application/json",status="201")

def api1(req,resp):
    if req.method == "GET":
        grupo = list(filter(lambda x : x[0] == req.url_match.group(1),status))
        if len(grupo) == 0:
            yield from picoweb.start_response(resp,"application/json",status="404")
            yield from resp.awrite(json.dumps({"code":404,"status":"group not found"}))
        else:
            yield from picoweb.start_response(resp,"application/json",status="200")
            yield from resp.awrite(json.dumps({"code":200,"grupo":grupo[0][0],"estado":grupo[0][1],"acciones":grupo[0][2],"label":grupo[0][3]}))
    else:
        yield from picoweb.start_response(resp,"application/json",status="405")
        yield from resp.awrite(json.dumps({"code":405,"status":"method not allowed on this path"}))
    
def api2(req,resp):
    if req.method == "POST":
        nombre = req.url_match.group(1)
        
        grupo = list(filter(lambda x : x[0] == nombre,status))
        accion = req.url_match.group(2)        
        
        
        if nombre == "control":
            #control commands
            if accion == "reset":
                yield from picoweb.start_response(resp,"application/json",status="200")
                yield from resp.awrite(json.dumps({"code":200,"status":"resetting"}))
                import machine
                machine.reset()
            elif accion == "consola":
                yield from picoweb.start_response(resp,"application/json",status="200")
                import webrepl
                webrepl.start()
                yield from resp.awrite(json.dumps({"code":200,"status":"webrelp launched"}))
        elif len(grupo) == 0:
            yield from picoweb.start_response(resp,"application/json",status="404")
            yield from resp.awrite(json.dumps({"code":404,"status":"group not found"}))
        else:
            if accion in grupo[0][2]:
                if accion == "on":
                    grupo[0][1] = 1
                elif accion == "off":
                    grupo[0][1] = 0
                
                if "Content-Type" in req.headers:
                    if req.headers["Content-Type"] == "application/json":
                        yield from picoweb.start_response(resp,"application/json",status="200")
                        yield from resp.awrite(json.dumps({"code":201,"grupo":grupo[0][0],"estado":grupo[0][1],"acciones":grupo[0][2],"label":grupo[0][3]}))
                else:
                    yield from picoweb.start_response(resp,status="302",headers={"Location":"/"})
                        
                        
            else:
                yield from picoweb.start_response(resp,"application/json",status="404")
                yield from resp.awrite(json.dumps({"code":404,"status":"acction not found"}))
                
    else:
        yield from picoweb.start_response(resp,"application/json",status="405")
        yield from resp.awrite(json.dumps({"code":405,"status":"method not allowed on this path"}))
        
    update_output()

def api3(req,resp):
    yield from picoweb.start_response(resp,status="201")
    
ROUTES = [
    ("/",index),
    ("/main.css",lambda req, resp: (yield from app.sendfile(resp, "/static/main.css",headers={"Cache-Control": "immutable"}))),
    ("/about",lambda req, resp: (yield from app.sendfile(resp, "/static/about.html",headers={"Cache-Control": "immutable"}))),
    (re.compile("^/api/?$"),api),
    (re.compile("^/api/([a-zA-z0-9]*)$"),api1),
    (re.compile("^/api/([a-zA-z0-9]*)/([a-zA-z0-9]*)$"),api2),
    (re.compile("^/api/([a-zA-z0-9]*)/([a-zA-z0-9]*)/([a-zA-z0-9]*)$"),api3)
]

app = picoweb.WebApp(__name__,ROUTES)
app.run(debug=1,host="0.0.0.0",port=80)
o
